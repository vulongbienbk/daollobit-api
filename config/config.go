package config

import (
	"daollobit-api/jwt"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	/*machinery config*/
	BrokerUrl        string `envconfig:"API_BROKER__URL" required:"true"`
	Queue            string `envconfig:"API_QUEUE" required:"true"`
	ResultBackendUrl string `envconfig:"API_RESULT_BACKEND_URL" required:"true"`

	/*redis config*/
	RedisUrl      string `envconfig:"API_REDIS_URL" required:"true"`
	RedisPassword string `envconfig:"API_REDIS_PASSWORD" required:"true"`

	/*mysql config*/
	MysqlUser     string `envconfig:"API_MYSQL_USER" required:"true"`
	MysqlPassword string `envconfig:"API_MYSQL_PASSWORD" required:"true"`
	MysqlHost     string `envconfig:"API_MYSQL_HOST" required:"true"`
	MysqlDatabase string `envconfig:"API_MYSQL_DATABASE" required:"true"`

	/*logger config*/
	LoggerDebugLog  bool   `envconfig:"API_LOGGER_DEBUG_LOG" required:"true"`
	LoggerLogToFile bool   `envconfig:"API_LOGGER_LOG_TO_FILE" required:"true"`
	LoggerLogPath   string `envconfig:"API_LOGGER_LOG_PATH" required:"true"`
	LoggerNoColor   bool   `envconfig:"API_LOGGER_NO_COLOR" required:"true"`

	/*address listen config*/
	ListenAddress string `envconfig:"API_LISTEN_ADDRESS" required:"true"`

	/*recaptcha config*/
	RecaptchaSiteKey   string `envconfig:"API_RECAPTCHA_SITE_KEY" required:"true"`
	RecaptchaSecretKey string `envconfig:"API_RECAPTCHA_SECRET_KEY" required:"true"`

	/*jwt config*/
	JwtSecretKey string `envconfig:"API_JWT_SECRET_KEY" required:"true"`
	JwtHelper    *jwt.JWTHelper

	/*add config for SendGrid*/
	SendGridSenderDefault string `envconfig:"SENDGRID_SENDER_DEFAULT" required:"true"`
	SendGridApiKey        string `envconfig:"SENDGRID_API_KEY" required:"true"`
	SendGridEndpoint      string `envconfig:"SENDGRID_ENDPOINT" required:"true"`
	SendGridHost          string `envconfig:"SENDGRID_HOST" required:"true"`
}

func NewConfig() (*Config, error) {
	var cnf Config
	err := envconfig.Process("", &cnf)
	if err != nil {
		return nil, err
	}

	return &cnf, nil
}
