package jwt

import (
	"crypto/rsa"
	"daollobit-api/models"
	"github.com/dgrijalva/jwt-go"
)

type JWTHelper struct {
	publicKey *rsa.PublicKey
}

func NewJWTHelper(jwtPublic string) (*JWTHelper, error) {
	pubKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(jwtPublic))
	if err != nil {
		return nil, err
	}

	return &JWTHelper{
		publicKey: pubKey,
	}, nil
}

func (helper *JWTHelper) ParseWithClaims(tokenString string, claims jwt.Claims) (*jwt.Token, error) {
	jwtParse := jwt.Parser{SkipClaimsValidation: false}
	return jwtParse.ParseWithClaims(
		tokenString,
		claims,
		func(token *jwt.Token) (interface{}, error) {
			return helper.publicKey, nil
		})
}

func (helper *JWTHelper) RefreshToken(refreshJwt models.JwtData) (string, error) {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshJwt)
	// Sign and get the complete encoded token as a string using the secret
	return token.SignedString(helper.publicKey)
}
