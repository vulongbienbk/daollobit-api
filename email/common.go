package email

type SendingData map[string]interface{}

func (data SendingData) getSender() string {
	return data.getString("sender", "")
}

func (data SendingData) getSenderName() string {
	return data.getString("sender_name", "")
}

func (data SendingData) getSubject() string {
	return data.getString("subject", "")
}

func (data SendingData) getTaskId() string {
	return data.getString("task_id", "")
}

func (data SendingData) setSubject(value string) {
	data.setString("subject", value)
}

func (data SendingData) getRecipients() []string {
	return data.getArrayString("recipients")
}

func (data SendingData) setRecipients(recipients []string) {
	data["recipients"] = recipients
}

func (data SendingData) getHtmlBody() string {
	return data.getString("html_body", "")
}

func (data SendingData) setHtmlBody(value string) {
	data.setString("html_body", value)
}

func (data SendingData) getTextBody() string {
	return data.getString("text_body", "")
}

func (data SendingData) setTextBody(value string) {
	data.setString("text_body", value)
}

func (data SendingData) getTemplateID() string {
	return data.getString("template_id", "")
}

func (data SendingData) getString(key string, defaultValue string) string {
	if data[key] == nil {
		return defaultValue
	}
	return data[key].(string)
}

func (data SendingData) setString(key string, value string) {
	if data[key] != nil {
		data[key] = value
	}
}

func (data SendingData) getArrayString(key string) []string {
	if data[key] == nil {
		return []string{}
	}
	val, ok := data[key].([]interface{})
	if !ok {
		return []string{}
	}
	var returnVal []string
	for _, v := range val {
		val, ok := v.(string)
		if ok {
			returnVal = append(returnVal, val)
		}
	}
	return returnVal
}
