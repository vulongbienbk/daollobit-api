package email

import (
	"errors"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"strings"
)

/*implement send email using SENDGRID provider*/
type SendGridEmail struct {
	sender   string
	apiKey   string
	endpoint string
	host     string
}

func (sgEmail *SendGridEmail) send(data SendingData) (string, error) {
	request := sendgrid.GetRequest(sgEmail.apiKey, sgEmail.endpoint, sgEmail.host)
	request.Method = "POST"
	body, err := sgEmail.buildBodyRequest(data)
	if err != nil {
		return "", err
	}
	request.Body = body
	response, err := sendgrid.API(request)
	if err != nil {
		return "", err
	}
	/*if success then return message_id of email*/
	if len(response.Headers["X-Message-Id"]) > 0 {
		return response.Headers["X-Message-Id"][0], nil
	}

	return "", errors.New(response.Body)
}

func (sgEmail *SendGridEmail) buildBodyRequest(data SendingData) ([]byte, error) {
	sender := data.getSender()
	if strings.TrimSpace(sender) == "" {
		sender = sgEmail.sender
	}

	/*create mail*/
	m := mail.NewV3Mail()
	/*create from info*/
	address := sender
	senderName := data.getSenderName()
	if strings.TrimSpace(senderName) == "" {
		senderName = sender
	}

	from := mail.NewEmail(senderName, address)
	m.SetFrom(from)
	/*set subject*/
	m.Subject = data.getSubject()
	p := mail.NewPersonalization()
	/*create tos info*/
	var tos []*mail.Email
	for _, v := range data.getRecipients() {
		tos = append(tos, mail.NewEmail(v, v))
	}
	p.AddTos(tos...)
	if data.getTemplateID() == "" {
		/*set content text body*/
		if len(data.getTextBody()) > 0 {
			c := mail.NewContent("text/plain", data.getTextBody())
			m.AddContent(c)
		}
		/*set content html body*/
		c := mail.NewContent("text/html", data.getHtmlBody())
		m.AddContent(c)
	} else {
		m.TemplateID = data.getTemplateID()
		for k, val := range data {
			p.SetDynamicTemplateData(k, val)
		}
	}
	m.AddPersonalizations(p)
	return mail.GetRequestBody(m), nil
}

func NewSendGridEmail(sender, key, endpoint, host string) (*SendGridEmail, error) {
	return &SendGridEmail{
		sender:   sender,
		apiKey:   key,
		endpoint: endpoint,
		host:     host,
	}, nil
}
