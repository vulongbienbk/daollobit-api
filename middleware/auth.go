package middleware

import (
	"daollobit-api/config"
	"daollobit-api/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func getAccessTokenFromRequest(context *gin.Context) string {
	auth := context.Request.Header.Get("Authorization")
	split := strings.SplitN(auth, " ", 2)
	if len(split) != 2 || !strings.EqualFold(split[0], "bearer") {
		return auth
	}
	return split[1]
}

func Auth(conf *config.Config) gin.HandlerFunc {
	return func(context *gin.Context) {
		tokenString := getAccessTokenFromRequest(context)
		var requestData models.JwtData
		token, err := conf.JwtHelper.ParseWithClaims(tokenString, &requestData)
		if err != nil {
			context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"meta": models.Meta{
					Code:    http.StatusBadRequest,
					Message: "parse JWT token error",
				},
			})
			return
		}
		if !token.Valid {
			context.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"meta": models.Meta{
					Code:    http.StatusUnauthorized,
					Message: "token is invalid error",
				},
			})
			return
		}

		context.Next()
		/*refresh token is valid for 1 hour*/
		refreshJwt := models.JwtData{
			UserID: requestData.UserID,
			Email:  requestData.Email,
			Exp:    "1h",
		}
		newToken, _ := conf.JwtHelper.RefreshToken(refreshJwt)
		context.Writer.Header().Set("token", newToken)
	}
}
