package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) listCurrencyPair(ctx context.Context) ([]*models.CurrencyPair, error) {
	span, ctx := apm.StartSpan(ctx, "listCurrencyPair", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getCurrencyPair(ctx context.Context, id int64) (*models.CurrencyPair, error) {
	span, ctx := apm.StartSpan(ctx, "getCurrencyPair", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getCurrencyPairBySymbol(ctx context.Context, symbol string) (*[]models.CurrencyPair, error) {
	span, ctx := apm.StartSpan(ctx, "getCurrencyPairBySymbol", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) createCurrencyPair(ctx context.Context, item *models.CurrencyPair) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createCurrencyPair", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) activateCurrencyPair(ctx context.Context, currencyPairId int64) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "activateCurrencyPair", "custom")
	defer span.End()

	return 0, nil
}

func (r *Repo) deactivateCurrencyPair(ctx context.Context, currencyPairId int64) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "deactivateCurrencyPair", "custom")
	defer span.End()

	return 0, nil
}
