package database

import (
	"context"
	"daollobit-api/models"
	"errors"
	"github.com/shopspring/decimal"
	"go.elastic.co/apm"
)

func (r *Repo) listBalance(ctx context.Context, userId int64) ([]*models.BalanceInfo, error) {
	span, ctx := apm.StartSpan(ctx, "listBalance", "custom")
	defer span.End()
	query := `select b.id, a.id as currency_id, a.symbol, b.balance, b.lock_balance, b.balance - b.lock_balance as available_balance
                     from currencies a
                     left join balances b
                     on b.user_id = ? and b.currency_id = a.id
                     where a.activate = 1`

	rows, err := r.db.QueryContext(ctx, query, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []*models.BalanceInfo
	for rows.Next() {
		var id int64
		var currencyId int64
		var symbol string
		var balance decimal.Decimal
		var lockBalance decimal.Decimal
		var availableBalance decimal.Decimal
		err = rows.Scan(&id, &currencyId, &symbol, &balance, &lockBalance, &availableBalance)
		if err != nil {
			return nil, err
		}

		item := &models.BalanceInfo{
			Id:               id,
			CurrencyId:       currencyId,
			Symbol:           symbol,
			Balance:          balance,
			LockBalance:      lockBalance,
			AvailableBalance: availableBalance,
		}
		result = append(result, item)
	}

	return result, nil
}

func (r *Repo) getUserCurrencyBalance(ctx context.Context, userId int64, currencyId int64) (*models.BalanceInfo, error) {
	span, ctx := apm.StartSpan(ctx, "getUserCurrencyBalance", "custom")
	defer span.End()
	query := `select b.id, a.id as currency_id, a.symbol, b.balance, b.lock_balance, b.balance - b.lock_balance as available_balance
                     from currencies a
                     left join balances b
                     on b.user_id = ? and b.currency_id = a.id
                     where a.activate = 1 and a.id = ?`

	rows, err := r.db.QueryContext(ctx, query, userId, currencyId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []*models.BalanceInfo
	for rows.Next() {
		var id int64
		var currencyId int64
		var symbol string
		var balance decimal.Decimal
		var lockBalance decimal.Decimal
		var availableBalance decimal.Decimal
		err = rows.Scan(&id, &currencyId, &symbol, &balance, &lockBalance, &availableBalance)
		if err != nil {
			return nil, err
		}

		item := &models.BalanceInfo{
			Id:               id,
			CurrencyId:       currencyId,
			Symbol:           symbol,
			Balance:          balance,
			LockBalance:      lockBalance,
			AvailableBalance: availableBalance,
		}
		result = append(result, item)
	}

	if len(result) > 0 {
		return result[0], nil
	}

	return nil, errors.New("no row")
}

func (r *Repo) createBalance(ctx context.Context, item *models.BalanceItem) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createBalance", "custom")
	defer span.End()
	result, err := r.db.ExecContext(ctx,
		`INSERT INTO balances(
						user_id,
						currency_id,
						balance,
						lock_balance,
						address) VALUES (?, ?, ?, ?, ?)`,
		item.UserId,
		item.CurrencyId,
		item.Balance,
		item.LockBalance,
		item.Address,
	)
	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (r *Repo) UpdateBalance(ctx context.Context, quantity decimal.Decimal, lock decimal.Decimal, item *models.BalanceItem) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "UpdateBalance", "custom")
	defer span.End()
	item.Balance = item.Balance.Add(quantity)
	item.LockBalance = item.LockBalance.Add(lock)

	if item.Balance.Sign() == -1 || item.LockBalance.Sign() == -1 {
		return 0, errors.New("no row updated")
	}
	query := `UPDATE balances 
                        SET balance = ?, lock_balance = ?
                        WHERE user_id = ? and currency_id = ?`
	result, err := r.db.Exec(query, item.Balance, item.LockBalance, item.UserId)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func (r *Repo) UpdateBalanceAddress(ctx context.Context, address string, balanceId int64) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "UpdateBalanceAddress", "custom")
	defer span.End()

	query := `UPDATE balances SET address = ? WHERE id = ?`
	result, err := r.db.Exec(query, address, balanceId)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}
