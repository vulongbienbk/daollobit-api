package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) listDeposit(ctx context.Context) ([]*models.Deposit, error) {
	span, ctx := apm.StartSpan(ctx, "listDeposit", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getDeposit(ctx context.Context, id int64) (*models.Deposit, error) {
	span, ctx := apm.StartSpan(ctx, "getDeposit", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) listUnconfirmedDeposit(ctx context.Context, currencyId int64) ([]*models.Deposit, error) {
	span, ctx := apm.StartSpan(ctx, "listUnconfirmedDeposit", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getConfirmedBalanceDeposit(ctx context.Context, userId int64) ([]*models.Deposit, error) {
	span, ctx := apm.StartSpan(ctx, "getConfirmedBalanceDeposit", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) createDeposit(ctx context.Context, item *models.Deposit) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createDeposit", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) createMultipleDeposit(ctx context.Context, items []*models.Deposit) error {
	span, ctx := apm.StartSpan(ctx, "createMultipleDeposit", "custom")
	defer span.End()
	//TODO
	return nil
}

func (r *Repo) updateConfirmDeposit(ctx context.Context, item *models.Deposit) error {
	span, ctx := apm.StartSpan(ctx, "updateConfirmDeposit", "custom")
	defer span.End()
	//TODO
	return nil
}

func (r *Repo) rejectDeposit(ctx context.Context, depositId int64) error {
	span, ctx := apm.StartSpan(ctx, "rejectDeposit", "custom")
	defer span.End()
	//TODO
	return nil
}
