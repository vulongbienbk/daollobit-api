package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) getUser(ctx context.Context, id int64) ([]*models.User, error) {
	span, ctx := apm.StartSpan(ctx, "getUser", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getUserByEmail(ctx context.Context, email string) (*models.User, error) {
	span, ctx := apm.StartSpan(ctx, "getUserByEmail", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) listUser(ctx context.Context, pattern string) ([]*models.User, error) {
	span, ctx := apm.StartSpan(ctx, "listUser", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) createUser(ctx context.Context, user *models.User) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createUser", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) updateUser(ctx context.Context, user *models.User) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "updateUser", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) getLoginHistory(ctx context.Context, userId int64) ([]*models.LoginHistory, error) {
	span, ctx := apm.StartSpan(ctx, "getLoginHistory", "custom")
	defer span.End()
	//TODO
	return nil, nil
}
