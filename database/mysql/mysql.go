/**
 * Created by Pham Quoc Viet on 2/26/2019
 */

package database

import (
	"daollobit-api/config"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/mysql"
	"time"
)

func NewMySQL(conf *config.Config) (*sql.DB, error) {
	mysqlConnectStr := fmt.Sprintf("%v:%v@tcp(%v)/%v", conf.MysqlUser, conf.MysqlPassword, conf.MysqlHost, conf.MysqlDatabase)
	mysqlDB, err := apmsql.Open("mysql", mysqlConnectStr)
	if err != nil {
		return nil, errors.Wrap(err, "fail when create mysql connection")
	}
	if err = mysqlDB.Ping(); err != nil {
		return nil, errors.Wrap(err, "fail when ping mysql")
	}
	/*limit connection mysql*/
	mysqlDB.SetMaxOpenConns(25)
	mysqlDB.SetMaxIdleConns(25)
	mysqlDB.SetConnMaxLifetime(5 * time.Minute)
	return mysqlDB, nil
}

type Repo struct {
	db *sql.DB
}

func NewRepo(db *sql.DB) *Repo {
	return &Repo{db: db}
}
