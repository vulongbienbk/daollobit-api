package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) CreateOrder(ctx context.Context, item *models.Order) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "CreateOrder", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) listOrder(ctx context.Context) ([]*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "listOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) openListOrder(ctx context.Context) (*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "openListOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) tradeListOrder(ctx context.Context, userId int64, currencyPairId int64, fromDate int64, toDate int64) ([]*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "tradeListOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) executionListOrder(ctx context.Context, currencyPairId int64, fromDate int64) ([]*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "executionListOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) executionBalanceOrder(ctx context.Context, userId int64) ([]*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "executionBalanceOrder", "custom")
	defer span.End()

	return nil, nil
}

func (r *Repo) lastExecutionOrder(ctx context.Context, currencyPairId int64) ([]*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "lastExecutionOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getOrder(ctx context.Context, orderId int64) (*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "getOrder", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) makeOrder(ctx context.Context, item *models.Order) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "makeOrder", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) cancelOrder(ctx context.Context, item *models.Order) (*models.Order, error) {
	span, ctx := apm.StartSpan(ctx, "cancelOrder", "custom")
	defer span.End()
	//TODO
	return nil, nil
}
