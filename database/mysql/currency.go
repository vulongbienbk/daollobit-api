package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) listCurrency(ctx context.Context) ([]*models.CurrencyItem, error) {
	span, ctx := apm.StartSpan(ctx, "listCurrency", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getCurrency(ctx context.Context, id int64) (*models.CurrencyItem, error) {
	span, ctx := apm.StartSpan(ctx, "getCurrency", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getCurrencyBySymbol(ctx context.Context, symbol string) (*[]models.CurrencyItem, error) {
	span, ctx := apm.StartSpan(ctx, "getCurrencyBySymbol", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) createCurrency(ctx context.Context, item *models.CurrencyItem) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createCurrency", "custom")
	defer span.End()
	//TODO
	return 0, nil
}

func (r *Repo) updateCurrency(ctx context.Context, item *models.CurrencyItem) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "updateCurrency", "custom")
	defer span.End()

	return 0, nil
}

func (r *Repo) activateCurrency(ctx context.Context, currencyId int64) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "activateCurrency", "custom")
	defer span.End()

	return 0, nil
}

func (r *Repo) deactivateCurrency(ctx context.Context, currencyId int64) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "deactivateCurrency", "custom")
	defer span.End()

	return 0, nil
}
