package database

import (
	"context"
	"daollobit-api/models"
	"go.elastic.co/apm"
)

func (r *Repo) listWithdrawal(ctx context.Context) ([]*models.Withdrawal, error) {
	span, ctx := apm.StartSpan(ctx, "listWithdrawal", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) listExpiredWithdrawal(ctx context.Context) ([]*models.Withdrawal, error) {
	span, ctx := apm.StartSpan(ctx, "listExpiredWithdrawal", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getWithdrawal(ctx context.Context, id int64) (*models.Withdrawal, error) {
	span, ctx := apm.StartSpan(ctx, "getWithdrawal", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) getConfirmBalanceWithdrawal(ctx context.Context, userId int64) (*models.Withdrawal, error) {
	span, ctx := apm.StartSpan(ctx, "getConfirmBalanceWithdrawal", "custom")
	defer span.End()
	//TODO
	return nil, nil
}

func (r *Repo) createWithdrawal(ctx context.Context, item *models.Withdrawal) (int64, error) {
	span, ctx := apm.StartSpan(ctx, "createWithdrawal", "custom")
	defer span.End()
	//TODO
	return 0, nil
}
