package redis

import (
	"github.com/go-redis/redis"
)

func NewRedis(redisUrl string, password string) (*redis.Client, error) {
	options, err := redis.ParseURL(redisUrl)
	if err != nil {
		return nil, err
	}
	options.Password = password
	client := redis.NewClient(options)
	if _, err := client.Ping().Result(); err != nil {
		return nil, err
	}
	return client, nil
}
