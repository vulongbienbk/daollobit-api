/**
 * Created by Pham Quoc Viet on 3/19/2019
 */

package logger

import (
	"daollobit-api/config"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmlogrus"
	"log"
	"os"
	"runtime"
)

var logRus = &logrus.Logger{
	Out:   os.Stderr,
	Hooks: make(logrus.LevelHooks),
	Level: logrus.DebugLevel,
	Formatter: &logrus.JSONFormatter{
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyTime:  "@timestamp",
			logrus.FieldKeyLevel: "log.level",
			logrus.FieldKeyMsg:   "message",
			logrus.FieldKeyFunc:  "function.name", // non-ECS
		},
	},
}

func init() {
	logRus.AddHook(&apmlogrus.Hook{})
	apm.DefaultTracer.SetLogger(logRus)
}

func getFileName(file string) string {
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			file = file[i+1:]
			break
		}
	}
	return file
}

func NewLogger(conf *config.Config) error {
	// --- Add File hook for default log
	log.SetFlags(log.LstdFlags + log.Lshortfile + log.LUTC)

	// --- Init file logger
	if conf.LoggerLogToFile {
		// --- Init folder to write file
		if _, err := os.Stat("./logs"); os.IsNotExist(err) {
			os.Mkdir("./logs", 0777)
		}

		f, err := os.OpenFile("./logs/"+conf.LoggerLogPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
		if err != nil {
			return err
		}
		logrus.SetOutput(f)
	} else {
		logrus.SetOutput(os.Stdout)
	}

	// --- Format and hook caller
	logrus.SetReportCaller(true)
	logrus.SetFormatter(&Formatter{
		NoColors: conf.LoggerNoColor || conf.LoggerLogToFile,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			filename := getFileName(f.File)
			function := getFileName(f.Function)
			return fmt.Sprintf("[%s()]", function), fmt.Sprintf("%s:%d", filename, f.Line)
		},
	})

	// --- Set debug log
	if conf.LoggerDebugLog {
		logrus.SetLevel(logrus.DebugLevel)
	}
	return nil
}

var (
	Println  = logrus.Println
	Printf   = logrus.Printf
	Info     = logrus.Info
	Infof    = logrus.Infof
	Warning  = logrus.Warning
	Warningf = logrus.Warningf
	Warn     = logrus.Warn
	Warnf    = logrus.Warnf
	Error    = logrus.Error
	Errorf   = logrus.Errorf
	Debug    = logrus.Debug
	Debugf   = logrus.Debugf
	Fatal    = log.Fatal
	Fatalf   = log.Fatalf
)
