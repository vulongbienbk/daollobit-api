/**
 * Created by Pham Quoc Viet on 3/19/2019
 */

package logger

import (
	"github.com/sirupsen/logrus"
	"bytes"
	"fmt"
	"runtime"
	"strings"
	"sort"
)

// Formatter implements logrus.Formatter interface.
type Formatter struct {
	NoColors bool
	CallerPrettyfier func(*runtime.Frame) (function string, file string)
}

// Format building log message.
func (f *Formatter) Format(entry *logrus.Entry) ([]byte, error) {
	outBuff := &bytes.Buffer{}

	// --- Write level
	if !f.NoColors {
		levelColor := getColorByLevel(entry.Level)
		fmt.Fprintf(outBuff, "\x1b[%dm", levelColor)
	}
	levelText := strings.ToUpper(entry.Level.String())
	levelText = levelText[0:4]
	fmt.Fprintf(outBuff, "%s", levelText)

	// --- Clear console color
	if !f.NoColors {
		outBuff.WriteString("\x1b[0m")
	}

	// --- Write time
	//outBuff.WriteString(entry.Time.UTC().Format("2006-01-02 15:04:05.000"))
	fmt.Fprintf(outBuff, "[%v]", entry.Time.UTC().Format("2006-01-02 15:04:05.000"))

	// --- Write context
	if len(entry.Data) != 0 {
		fields := make([]string, 0, len(entry.Data))
		for field := range entry.Data {
			fields = append(fields, field)
		}

		sort.Strings(fields)

		for _, field := range fields {
			fmt.Fprintf(outBuff, " [%v]", entry.Data[field])
		}
	}

	// --- Find Caller
	var funcVal string
	if entry.HasCaller() {
		var fileVal string
		if f.CallerPrettyfier != nil {
			funcVal, fileVal = f.CallerPrettyfier(entry.Caller)
		} else {
			funcVal = entry.Caller.Function
			fileVal = fmt.Sprintf("%s:%d", entry.Caller.File, entry.Caller.Line)
		}
		fmt.Fprintf(outBuff, " %s:", fileVal)
	}

	// Remove a single newline if it already exists in the message to keep
	// the behavior of logrus text_formatter the same as the stdlib log package
	entry.Message = strings.TrimSuffix(entry.Message, "\n")
	fmt.Fprintf(outBuff, " %s", entry.Message)

	if funcVal != "" {fmt.Fprintf(outBuff, " %s", funcVal)}

	outBuff.WriteByte(10)

	return outBuff.Bytes(), nil
}

const (
	colorRed    = 31
	colorYellow = 33
	colorBlue   = 36
	colorGray   = 37
)

func getColorByLevel(level logrus.Level) int {
	switch level {
	case logrus.DebugLevel:
		return colorGray
	case logrus.WarnLevel:
		return colorYellow
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		return colorRed
	default:
		return colorBlue
	}
}