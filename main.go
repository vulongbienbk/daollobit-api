package main

import (
	"daollobit-api/config"
	database "daollobit-api/database/mysql"
	"daollobit-api/email"
	"daollobit-api/logger"
	"daollobit-api/routers"
	"daollobit-api/server"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"log"
)

func main() {
	/*init config*/
	conf, err := config.NewConfig()
	if err != nil {
		log.Panic(nil, err)
	}

	err = logger.NewLogger(conf)
	if err != nil {
		log.Panic(nil, err)
	}

	mysqlInstance, err := database.NewMySQL(conf)
	if err != nil {
		log.Panic(nil, err)
	}
	defer func() {
		mysqlInstance.Close()
	}()

	/*init + check redis connection*/
	options, err := redis.ParseURL(conf.RedisUrl)
	if err != nil {
		log.Panic(nil, err)
	}
	options.Password = conf.RedisPassword
	redisInstance := redis.NewClient(options)
	defer redisInstance.Close()

	if _, err := redisInstance.Ping().Result(); err != nil {
		log.Panic(nil, err)
	}

	/*init task server*/
	taskServer, err := server.NewTaskServer(conf.BrokerUrl, conf.Queue, conf.ResultBackendUrl)
	if err != nil {
		log.Panic(nil, err)
	}

	/*init email sendgrid*/
	sendGrid, err := email.NewSendGridEmail(conf.SendGridSenderDefault, conf.SendGridApiKey, conf.SendGridEndpoint, conf.SendGridHost)
	if err != nil {
		log.Panic(nil, err)
	}
	_, err = server.NewMatchingEngine(conf, mysqlInstance, redisInstance, taskServer, sendGrid)
	if err != nil {
		log.Panic(nil, err)
	}

	/*apis*/
	router := gin.New()
	r := routers.NewRouters(conf, router, redisInstance)
	r.SetupRouter()

	if err = r.Run(); err != nil {
		log.Panic(nil, err)
	}
}
