module daollobit-api

go 1.14

require (
	github.com/RichardKnop/machinery v1.8.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.6.0
	go.elastic.co/apm v1.8.0
	go.elastic.co/apm/module/apmgin v1.8.0
	go.elastic.co/apm/module/apmlogrus v1.8.0
	go.elastic.co/apm/module/apmsql v1.8.0
)
