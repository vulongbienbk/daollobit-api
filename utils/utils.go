package utils

import (
	"daollobit-api/constant"
)

var (
	pairIds = []int64{constant.PairIdBtcKrw, constant.PairIdEthKrw, constant.PairIdMrnKrw}
)
