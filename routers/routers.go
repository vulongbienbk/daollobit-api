package routers

import (
	"daollobit-api/config"
	"daollobit-api/middleware"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"go.elastic.co/apm/module/apmgin"
	"net/http"
)

type Routers struct {
	redisDb *redis.Client
	router  *gin.Engine
	conf    *config.Config
}

func (r *Routers) SetupRouter() {
	r.router.Use(
		apmgin.Middleware(r.router),
		middleware.Auth(r.conf),
	)
	r.router.GET("/health", r.healthCheck)

	// public API
	r.router.GET("/orderbook", r.getOrderBook)
	r.router.GET("/execution_list", r.getExecutionList)

	// group: auth
	auth := r.router.Group("/auth")
	{
		auth.POST("/login", r.login)
		auth.POST("/register", r.register)
		auth.POST("/active_user", r.activeUser)
		auth.POST("/forgot_password", r.forgotPassword)
		auth.POST("/reset_password", r.resetPassword)
		auth.GET("/register/active_user", r.registerActiveUser)
	}

	// group: balance
	balance := r.router.Group("/balance")
	{
		balance.GET("/get", r.getBalance)
		balance.GET("/get_address", r.getAddressBalance)
		balance.GET("/list", r.listBalance)
		balance.GET("/change_address", r.changeAddressBalance)
	}

	// group: currency
	currency := r.router.Group("/currency")
	{
		currency.GET("/get", r.getCurrency)
		currency.GET("/list", r.listCurrency)
	}

	// group: currency_pair
	currencyPair := r.router.Group("/currency_pair")
	{
		currencyPair.GET("/get", r.getCurrencyPair)
		currencyPair.GET("/list", r.listCurrencyPair)
	}

	// group: deposit
	deposit := r.router.Group("/deposit")
	{
		deposit.GET("/get", r.getDeposit)
		deposit.GET("/list", r.listDeposit)
	}

	// group: order
	order := r.router.Group("/order")
	{
		order.GET("/list", r.listOrder)
		order.GET("/open_list", r.openListOrder)
		order.GET("/trade_list", r.tradeListOrder)
		order.GET("/get", r.getOrder)
		order.GET("/make_order", r.makeOrder)
		order.GET("/cancel_order", r.cancelOrder)
	}

	// group: user
	user := r.router.Group("/user")
	{
		user.POST("/change_password", r.changePasswordUser)
		user.GET("/get", r.getUser)
		user.POST("/update_profile", r.updateProfileUser)
	}

	// group: withdrawal
	withdrawal := r.router.Group("/withdrawal")
	{
		withdrawal.GET("/list", r.listWithdrawal)
		withdrawal.GET("/get", r.getWithdrawal)
		withdrawal.POST("/request", r.requestWithdrawal)
		withdrawal.POST("/confirm", r.confirmWithdrawal)
		withdrawal.POST("/confirm", r.cancelWithdrawal)
	}
}

func (r *Routers) Run() error {
	return r.router.Run(r.conf.ListenAddress)
}

func NewRouters(cfg *config.Config, router *gin.Engine, redis *redis.Client) *Routers {
	return &Routers{
		redisDb: redis,
		conf:    cfg,
		router:  router,
	}
}

func (r *Routers) healthCheck(ctx *gin.Context) {
	/*check redis status*/
	_, err := r.redisDb.Ping().Result()
	if err != nil {
		ctx.JSON(
			http.StatusInternalServerError, gin.H{
				"code":    http.StatusInternalServerError,
				"message": "redis db die: " + err.Error(),
			},
		)
		return
	}
	/**if every thing is OK*/
	ctx.JSON(
		http.StatusOK, gin.H{
			"code":    http.StatusOK,
			"message": "OK",
		},
	)
}
