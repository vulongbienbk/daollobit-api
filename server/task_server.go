package server

import (
	"daollobit-api/config"
	"daollobit-api/email"
	"database/sql"
	"github.com/RichardKnop/machinery/v1"
	machineryConf "github.com/RichardKnop/machinery/v1/config"
	"github.com/RichardKnop/machinery/v1/tasks"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

func NewTaskServer(broker, queue, resultBackend string) (*machinery.Server, error) {
	var cnf = machineryConf.Config{
		Broker:        broker,
		DefaultQueue:  queue,
		ResultBackend: resultBackend,
	}

	server, err := machinery.NewServer(&cnf)
	if err != nil {
		return server, errors.Wrap(err, "create task server fail")
	}
	return server, nil
}

type MatchingEngine struct {
	config     *config.Config
	db         *sql.DB
	redis      *redis.Client
	taskServer *machinery.Server
	sgEmail    *email.SendGridEmail
}

func NewMatchingEngine(
	conf *config.Config,
	db *sql.DB,
	redis *redis.Client,
	taskServer *machinery.Server,
	sgEmail *email.SendGridEmail,
) (*MatchingEngine, error) {
	return &MatchingEngine{
		config:     conf,
		db:         db,
		redis:      redis,
		taskServer: taskServer,
		sgEmail:    sgEmail,
	}, nil
}

func (m *MatchingEngine) CreateTaskWithUuid(taskName string, uuid string, value string, retryCount int) *tasks.Signature {
	task := tasks.Signature{
		UUID: uuid,
		Name: taskName,
		Args: []tasks.Arg{
			{
				Type:  "string",
				Value: value,
			},
		},
		RetryCount: retryCount,
	}
	return &task
}
