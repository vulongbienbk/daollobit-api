package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type CurrencyItem struct {
	Id                int64           `json:"id"`
	Symbol            string          `json:"symbol"`
	Name              string          `json:"name"`
	IsCrypto          bool            `json:"is_crypto"`
	IsToken           bool            `json:"is_token"`
	AllowDeposit      bool            `json:"allow_deposit"`
	DepositFee        decimal.Decimal `json:"deposit_fee"`
	AllowWithdrawal   bool            `json:"allow_withdrawal"`
	WithdrawalFee     decimal.Decimal `json:"withdrawal_fee"`
	BlockConfirms     int             `json:"block_confirms"`
	BlockHeightSynced int             `json:"block_height_synced"`
	Decimal           int             `json:"decimal"`
	Active            bool            `json:"active"`
	Currenciescol     string          `json:"currenciescol"`
	CreateTime        time.Time       `json:"create_time"`
	UpdateTime        time.Time       `json:"update_time"`
}
