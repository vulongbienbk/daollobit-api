package models

import "time"

type User struct {
	Id               int64     `json:"id"`
	Email            string    `json:"email"`
	PasswordHash     string    `json:"password_hash"`
	Name             string    `json:"name"`
	Photo            string    `json:"photo"`
	Birthday         int64     `json:"birthday"`
	Gender           int       `json:"gender"`
	TwoFactoryEnable int       `json:"two_factory_enable"`
	TwoFactoryKey    string    `json:"two_factory_key"`
	Language         string    `json:"language"`
	Level            int       `json:"level"`
	Status           int       `json:"status"`
	Phone            string    `json:"phone"`
	CreateTime       time.Time `json:"create_time"`
	UpdateTime       time.Time `json:"update_time"`
}
