package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type ExecutionOrder struct {
	Id          int64           `json:"id"`
	ExecutionId int64           `json:"execution_id"`
	OrderId     int64           `json:"order_id"`
	Fee         decimal.Decimal `json:"fee"`
	FeeCurrency int64           `json:"fee_currency"`
	CreateTime  time.Time       `json:"create_time"`
	UpdateTime  time.Time       `json:"update_time"`
}
