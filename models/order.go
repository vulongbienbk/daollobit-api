package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Order struct {
	Id               int64           `json:"id"`
	UserId           int64           `json:"user_id"`
	CurrencyPairId   int64           `json:"currency_pair_id"`
	Side             int             `json:"side"`
	Type             int             `json:"type"`
	Quantity         decimal.Decimal `json:"quantity"`
	FilledQuantity   decimal.Decimal `json:"filled_quantity"`
	Price            decimal.Decimal `json:"price"`
	AvgPrice         decimal.Decimal `json:"avg_price"`
	RemainingBalance decimal.Decimal `json:"remaining_balance"`
	Status           int             `json:"status"`
	CreateTime       time.Time       `json:"create_time"`
	UpdateTime       time.Time       `json:"update_time"`
}
