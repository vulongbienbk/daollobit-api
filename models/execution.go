package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Execution struct {
	Id             int64           `json:"id"`
	CurrencyPairId int64           `json:"currency_pair_id"`
	Quantity       decimal.Decimal `json:"quantity"`
	Price          decimal.Decimal `json:"price"`
	Side           int             `json:"side"`
	TakerUserId    int64           `json:"taker_user_id"`
	MakerUserId    int64           `json:"maker_user_id"`
	CreateTime     time.Time       `json:"create_time"`
	UpdateTime     time.Time       `json:"update_time"`
}
