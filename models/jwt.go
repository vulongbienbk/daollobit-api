package models

import "github.com/dgrijalva/jwt-go"

type JwtData struct {
	jwt.StandardClaims
	Exp    string `json:"expiresIn"`
	Email  string `json:"email"`
	UserID string `json:"user_id"`
}

type Meta struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
