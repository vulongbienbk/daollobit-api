package models

type LoginHistory struct {
	Id        int64  `json:"id"`
	UserId    int64  `json:"user_id"`
	Ip        string `json:"ip"`
	Browser   string `json:"browser"`
	Os        string `json:"os"`
	Device    string `json:"device"`
	Location  string `json:"location"`
	Timestamp int64  `json:"timestamp"`
}
