package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Deposit struct {
	Id            int64           `json:"id"`
	UserId        int64           `json:"user_id"`
	CurrencyId    int64           `json:"currency_id"`
	Quantity      decimal.Decimal `json:"quantity"`
	Fee           decimal.Decimal `json:"fee"`
	TransactionId string          `json:"transaction_id"`
	Confirm       int             `json:"confirm"`
	ToAddress     string          `json:"to_address"`
	Status        int             `json:"status"`
	Price         decimal.Decimal `json:"price"`
	CreateTime    time.Time       `json:"create_time"`
	UpdateTime    time.Time       `json:"update_time"`
}
