package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type CurrencyPair struct {
	Id            int64           `json:"id"`
	Symbol        string          `json:"symbol"`
	Base          int64           `json:"base"`
	Quote         int64           `json:"quote"`
	Digit         int             `json:"digit"`
	Activate      int             `json:"activate"`
	MinStepAmount decimal.Decimal `json:"min_step_amount"`
	MinStepPrice  decimal.Decimal `json:"min_step_price"`
	CreateTime    time.Time       `json:"create_time"`
	UpdateTime    time.Time       `json:"update_time"`
}
