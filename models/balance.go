package models

import (
	"github.com/shopspring/decimal"
)

type BalanceInfo struct {
	Id               int64           `json:"id"`
	CurrencyId       int64           `json:"currency_id"`
	Symbol           string          `json:"symbol"`
	Balance          decimal.Decimal `json:"balance"`
	LockBalance      decimal.Decimal `json:"lock_balance"`
	AvailableBalance decimal.Decimal `json:"available_balance"`
}

type BalanceItem struct {
	UserId      int64           `json:"user_id"`
	CurrencyId  int64           `json:"currency_id"`
	Balance     decimal.Decimal `json:"balance"`
	LockBalance decimal.Decimal `json:"lock_balance"`
	Address     string          `json:"address"`
}