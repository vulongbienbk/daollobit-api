package models

import "time"

type Address struct {
	Id         int64     `json:"id"`
	Symbol     string    `json:"symbol"`
	UserId     int64     `json:"user_id"`
	Address    string    `json:"address"`
	PrivateKey string    `json:"private_key"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}
