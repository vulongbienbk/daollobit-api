package constant

const (
	OrderSideBuy  = 1
	OrderSideSell = 2

	OrderTypeLimit  = 1
	OrderTypeMarket = 2
	OrderTypeStop   = 3

	OrderStatusOpen     = 1
	OrderStatusFilled   = 2
	OrderStatusCanceled = 3

	OrderMsgTypeNew    = 1
	OrderMsgTypeCancel = 2

	ExecutionMsgTypeNew    = 1
	ExecutionMsgTypeCancel = 2
)

const (
	ProcessExecutionTask string = "processExecution"
	PrefixExecutionTask string = "task_execution_"
)

const (
	PairIdBtcKrw int64 = 1
	PairIdEthKrw int64 = 2
	PairIdMrnKrw int64 = 3
)
